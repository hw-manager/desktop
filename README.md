# HW-Manager

## What is this project for?

With the application 'HW-Manager' you are able to save your homework for
school.

## TODO

Take a look at
[GitHub Issues](https://github.com/hw-manager/desktop/issues).

## LICENSE

See [LICENSE](./LICENSE).
