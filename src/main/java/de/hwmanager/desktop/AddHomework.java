package de.hwmanager.desktop;
/*
* @author Nico Alt
* @author Devin
* See the file "LICENSE" for the full license governing this code.
*/

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

public class AddHomework extends JPanel {

	private static final long serialVersionUID = 5461173836895620167L;

	protected JTextField homework;
	protected JLabel homeworkLabel;
	protected JTextArea description;
	protected JLabel descriptionLabel;

	private UtilDateModel model;
	private JDatePanelImpl datePanel;
	private JDatePickerImpl datePicker;
	private JLabel untilLabel;
	private String[] labels = { R.string("biology"), R.string("chemistry"),
			R.string("english"), R.string("history"), R.string("math"),
			R.string("physics"), R.string("sports") };
	private JComboBox<String> subjectSpinner;
	private JLabel subjectLabel;

	public AddHomework() {
		super(new GridBagLayout());

		subjectLabel = new JLabel(R.string("subject"));
		subjectSpinner = new JComboBox<>(labels);
		subjectLabel.setLabelFor(subjectSpinner);
		homeworkLabel = new JLabel(R.string("homework"));
		homework = new JTextField(20);
		homeworkLabel.setLabelFor(homework);

		descriptionLabel = new JLabel(R.string("description"));
		description = new JTextArea(5, 20);
		description.setLineWrap(true);
		descriptionLabel.setLabelFor(description);

		JCheckBox importantCheck = new JCheckBox(R.string("important"));
		importantCheck.setSelected(false);

		JScrollPane scrollPane = new JScrollPane(description);
		JButton saveButton = new JButton(R.string("save"));
		saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				save();
			}
		});
		untilLabel = new JLabel(R.string("until"));

		model = new UtilDateModel();
		datePanel = new JDatePanelImpl(model);
		datePicker = new JDatePickerImpl(datePanel);
		untilLabel.setLabelFor(datePicker);

		// Add Components to this panel.
		GridBagConstraints c = new GridBagConstraints();
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.insets = new Insets(3, 3, 3, 3);

		c.fill = GridBagConstraints.HORIZONTAL;
		add(homeworkLabel, c);
		add(homework, c);
		add(subjectLabel, c);
		add(subjectSpinner, c);
		add(untilLabel, c);
		add(datePicker, c);
		add(descriptionLabel, c);

		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1.0;
		c.weighty = 1.0;
		add(scrollPane, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		add(importantCheck, c);
		add(saveButton, c);
	}

	/**
	 * Create the GUI and show it. For thread safety, this method should be
	 * invoked from the event dispatch thread.
	 */
	private static void createAndShowGUI() {
		// Create and set up the window.
		JFrame frame = new JFrame(R.string("hwAdd"));

		// Add contents to the window.
		frame.add(new AddHomework());

		// Display the window.
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	private void save() {
		// TODO: Do something.
		System.exit(0);
	}

	public static void main(String[] args) {
		// Use native menus on OS X
		if (System.getProperty("os.name").startsWith("Mac"))
			System.setProperty("apple.laf.useScreenMenuBar", "true");

		// Schedule a job for the event dispatch thread:
		// creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager
							.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException | InstantiationException
						| IllegalAccessException
						| UnsupportedLookAndFeelException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				createAndShowGUI();
			}
		});
	}
}