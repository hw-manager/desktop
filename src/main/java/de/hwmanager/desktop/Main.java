package de.hwmanager.desktop;
/*
* @author Nico Alt
* @author Devin
* See the file "LICENSE" for the full license governing this code.
*/

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class Main implements ActionListener {

	private static JFrame mainFrame;
	private static JMenuBar mainBar;
	private static JList<String> list;
	private static JPanel panel;

	public static void main(String[] args) {
		// Use native menus on OS X
		if (System.getProperty("os.name").startsWith("Mac"))
			System.setProperty("apple.laf.useScreenMenuBar", "true");

		initial();
	}

	private static void initial() {
		mainFrame = new JFrame(R.string("homeworkTitle"));
		mainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		mainBar = new JMenuBar();
		mainBar.add(menu(R.string("menuFile"), R.array("menuFileItems")));
		mainBar.add(menu(R.string("menuHomework"), R.array("menuHomeworkItems")));
		mainBar.add(menu(R.string("menuSubjects"), R.array("menuSubjectsItems")));
		mainFrame.setJMenuBar(mainBar);
		panel = new JPanel();
		panel.setLayout(new BorderLayout());
		mainFrame.getContentPane().add(panel);
		list = new JList<String>();
		panel.add(list, BorderLayout.CENTER);
		mainFrame.setVisible(true);
	}

	private static void addHomework() {
		AddHomework.main(null);
	}

	private static JMenu menu(String title, String[] items) {
		JMenu menu = new JMenu(title);
		if (items != null)
			for (int i = 0; i < items.length; i++) {
				JMenuItem item = new JMenuItem(items[i]);
				item.addActionListener(new Main());
				menu.add(item);
			}
		return menu;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Add":
			addHomework();
			break;
		case "New Window":
			initial();
		default:
			break;
		}

	}
}