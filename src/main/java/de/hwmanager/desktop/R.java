package de.hwmanager.desktop;
/*
* @author Nico Alt
* @author Devin
* See the file "LICENSE" for the full license governing this code.
*/

import java.util.Locale;
import java.util.ResourceBundle;

public class R {
	public static Locale[] supportedLocales = { Locale.GERMAN, Locale.ENGLISH };

	public static String string(String key) {
		return ResourceBundle.getBundle("resources.ApplicationResources",
				Locale.getDefault()).getString(key);
	}

	public static String[] array(String key) {
		return ResourceBundle
				.getBundle("resources.ApplicationResources",
						Locale.getDefault()).getString(key).split(",");
	}
}
